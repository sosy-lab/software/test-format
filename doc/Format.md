<!--
This file is part of the Test-Comp test format,
an exchange format for test suites:
https://gitlab.com/sosy-lab/software/test-format

SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Exchange Format for Test Suites

The goal of the exchange format is to facilitate
easy information exchange between different
automatic test-case generation tools
and towards test-case executors.
The format should allow users to specify test cases
in a both human- and machine-readable format that can be easily reused.

### Format

A test suite is defined as a folder `test-suite` that contains multiple files that describe
metadata and test cases.
The format expects one file `metadata.xml` for the metadata,
and an individual file with an arbitrary name matching `*.xml` for each test case.
The metadata format is described by [test-metadata.dtd](../test-metadata.dtd).
The test-case format is describd by [testcase.dtd](../testcase.dtd).
All files should be in the same directory.
Examples can be found [here](../examples/).

#### Semantics of a test case
Each test case consists of a sequence of `input` tags
that describe a test vector:
The first element of the test vector is the content of the first `input`
tag,
the second element of the test vector is the content of the second `input`
tag, and so on.

It is possible to specify two additional, optional information:
(1) the input type, which must be parsable as a C type in the corresponding program, 
and (2) the input variable through which the input is received.
If an input variable is specified, and the sequence of `inputs`
does not fit that information, the interpretation is undefined.

### Builder

The python module `tsbuilder` with class `tsbuilder.Builder` can be used
to create test-format xml files programmatically.
