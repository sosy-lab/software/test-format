// SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

int main() {
  int x = __VERIFIER_nondet_int();
  if (x > 0) {
    x++;
  } else {
    x--;
  }
}
