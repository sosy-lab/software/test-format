# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import glob
import logging
import os
import subprocess

from adaptors import utils
from adaptors.testcase_converter import TestConverter

MODULE_DIR = os.path.dirname(os.path.realpath(__file__))
BIN_DIR = os.path.join(MODULE_DIR, "klee/bin")
TESTS_DIR = "klee-tests"


class Preprocessor:
    def prepare_file(self, filename, error_method=None):
        if error_method:
            excludes = [error_method]
        else:
            excludes = ()
        with open(filename, "r", encoding="UTF-8") as inp:
            filecontent = inp.read()
        nondet_methods = utils.find_nondet_methods(filecontent, excludes=excludes)
        return self.prepare(filecontent, nondet_methods, error_method)

    def prepare(self, filecontent, nondet_methods_used, error_method=None):
        content = filecontent
        content += "\n"
        content += utils.EXTERNAL_DECLARATIONS
        content += "\n"
        content += utils.get_assume_method()
        if error_method:
            content += utils.get_error_method_definition(error_method)
        for method in nondet_methods_used:
            # append method definition at end of file content
            nondet_method_definition = self._get_nondet_method_definition(
                method["name"], method["type"], method["params"]
            )
            content += nondet_method_definition
        return content

    @staticmethod
    def _get_nondet_method_definition(method_name, method_type, param_types):
        var_name = utils.get_sym_var_name(method_name)
        method_head = utils.get_method_head(method_name, method_type, param_types)
        method_body = ["{"]
        if method_type != "void":
            method_body += [
                method_type + " " + var_name + ";",
                'klee_make_symbolic(&{0}, sizeof({0}), "{0}");'.format(var_name),
                "return {0};".format(var_name),
            ]
        method_body = "\n    ".join(method_body)
        method_body += "\n}\n"

        return method_head + method_body


class KleeTestConverter(TestConverter):
    def get_test_cases_in_dir(self, directory=None, exclude=()):
        if directory is None:
            directory = TESTS_DIR
        all_tests = glob.glob(directory + "/*.ktest")
        tcs = []
        for t in (t for t in all_tests if self._get_test_name(t) not in exclude):
            tcs.append(self.get_test_case_from_file(t))
        return tcs

    def get_test_case_from_file(self, test_file):
        file_name = self._get_test_name(test_file)
        with open(test_file, mode="rb") as inp:
            content = inp.read()
        return utils.TestCase(file_name, test_file, content)

    def get_test_vector(self, test_case):
        def _get_value(single_line):
            var_name = single_line.split(":")[2].strip()
            prefix_end = var_name.find("'")
            var_name = var_name[prefix_end + 1 : -1]
            return var_name

        def _convert_bytestring_to_hex(bytestring):
            byte_sequence = bytestring.split(r"\x")
            value = "".join(reversed(byte_sequence))
            value = "0x" + value
            return value

        ktest_tool = [os.path.join(BIN_DIR, "ktest-tool")]
        exec_output, _ = self._execute(ktest_tool + [test_case.origin])
        test_info = exec_output.split("\n")
        vector = utils.TestVector(test_case.name, test_case.origin)
        last_value = None
        for line in [l for l in test_info if l.startswith("object")]:
            logging.debug("Looking at line: %s", line)
            if "data:" in line:
                # we get the value in bytestring notation, so we have to convert it afterwards
                value = _get_value(line)
                value = _convert_bytestring_to_hex(value)
                assert last_value is None
                last_value = str(value)
            if last_value is not None:
                vector.add(last_value)
                last_value = None

        return vector

    @staticmethod
    def _execute(command):
        with subprocess.Popen(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=False,
        ) as process:
            std_outp, err_outp = process.communicate()
        return std_outp.decode(), err_outp

    @staticmethod
    def _get_test_name(test_file):
        return os.path.basename(test_file).split(".")[0]
