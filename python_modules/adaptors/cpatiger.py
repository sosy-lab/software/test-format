# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os

from adaptors import utils
from adaptors.testcase_converter import TestConverter

TESTS_DIR = "output"
INPUT_METHOD_NAME = "input"


class Preprocessor:
    def prepare_file(self, filename, error_method=None):
        if error_method:
            excludes = [error_method]
        else:
            excludes = ()
        with open(filename, "r", encoding="UTF-8") as inp:
            filecontent = inp.read()
        nondet_methods = utils.find_nondet_methods(filecontent, excludes=excludes)
        return self.prepare(filecontent, nondet_methods, error_method)

    def prepare(self, filecontent, nondet_methods_used, error_method=None):
        content = filecontent
        content += "\n"
        content += utils.EXTERNAL_DECLARATIONS
        content += "\n"
        content += utils.get_assume_method()
        content += "\n"
        content += "extern int input();\n"
        content += ""
        if error_method:
            content += utils.get_error_method_definition(error_method)
        for method in nondet_methods_used:
            # append method definition at end of file content
            nondet_method_definition = self._get_nondet_method_definition(
                method["name"], method["type"], method["params"]
            )
            content += nondet_method_definition

        return content

    @staticmethod
    def _get_nondet_method_definition(method_name, method_type, param_types):
        method_head = utils.get_method_head(method_name, method_type, param_types)
        method_body = ["{"]
        if method_type != "void":
            method_body += [
                "return ({0}) {1}();".format(method_type, INPUT_METHOD_NAME)
            ]
        method_body = "\n    ".join(method_body)
        method_body += "\n}\n"

        return method_head + method_body


class CpaTigerTestConverter(TestConverter):
    def get_test_cases_in_dir(self, directory=None, exclude=()):
        if directory is None:
            directory = TESTS_DIR
        tests_file = os.path.join(directory, "testsuite.txt")
        if os.path.exists(tests_file):
            with open(tests_file, "r", encoding="UTF-8") as inp:
                tests = [
                    l.strip()
                    for l in inp.readlines()
                    if l.strip().startswith("[") and l.strip().endswith("]")
                ]
            tests = [t for i, t in enumerate(tests) if str(i) not in exclude]
            tcs = []
            for i, t in enumerate(tests):
                tcs.append(utils.TestCase(str(i), tests_file, t))
            return tcs
        return []

    def get_test_case_from_file(self, test_file):
        """
        Not supported. It is not possible to create a single test case.

        see _get_test_cases_in_dir instead.

        :raises NotImplementedError: when called
        """
        raise NotImplementedError(
            "CPATiger can only create test cases for the full test suite"
        )

    def get_test_vector(self, test_case):
        assert len(test_case.content.split("\n")) == 1
        assert test_case.content.startswith("[") and test_case.content.endswith("]")
        test_vector = utils.TestVector(test_case.name, test_case.origin)
        processed_line = test_case.content[1:-1]
        test_values = processed_line.split(", ")
        for value in test_values:
            test_vector.add(value)
        return test_vector
