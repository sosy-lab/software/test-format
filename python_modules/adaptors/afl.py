# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import glob
import os

from adaptors import utils
from adaptors.testcase_converter import TestConverter


class Preprocessor:
    def prepare_file(self, filename, error_method=None):
        if error_method:
            excludes = [error_method]
        else:
            excludes = ()
        with open(filename, "r", encoding="UTF-8") as inp:
            filecontent = inp.read()
        nondet_methods = utils.find_nondet_methods(filecontent, excludes=excludes)
        return self.prepare(filecontent, nondet_methods, error_method)

    def prepare(self, filecontent, nondet_methods_used, error_method=None):
        content = filecontent
        content += "\n"
        content += utils.EXTERNAL_DECLARATIONS
        content += "\n"
        content += utils.get_assume_method()
        content += "\n"
        content += self._get_vector_read_method()
        if error_method:
            content += utils.get_error_method_definition(error_method)
        for method in nondet_methods_used:
            # append method definition at end of file content
            nondet_method_definition = self._get_nondet_method_definition(
                method["name"], method["type"], method["params"]
            )
            content += nondet_method_definition
        return content

    @staticmethod
    def _get_vector_read_method():
        return """char * parse_inp(char * __inp_var) {
        unsigned int input_length = strlen(__inp_var)-1;
        /* Remove '\\n' at end of input */
        if (__inp_var[input_length] == '\\n') {
            __inp_var[input_length] = '\\0';
        }

        char * parseEnd;
        char * value_pointer = malloc(16);

        unsigned long long intVal = strtoull(__inp_var, &parseEnd, 0);
        if (*parseEnd != 0) {
          long long sintVal = strtoll(__inp_var, &parseEnd, 0);
          if (*parseEnd != 0) {
            long double floatVal = strtold(__inp_var, &parseEnd);
            if (*parseEnd != 0) {
              fprintf(stderr, "Can't parse input: '%s' (failing at '%s')\\n", __inp_var, parseEnd);
              abort();

            } else {
              memcpy(value_pointer, &floatVal, 16);
            }
          } else {
            memcpy(value_pointer, &sintVal, 8);
          }
        } else {
          memcpy(value_pointer, &intVal, 8);
        }

        return value_pointer;
    }\n\n"""

    @staticmethod
    def _get_nondet_method_definition(method_name, method_type, method_param):
        definition = ""
        definition += utils.get_method_head(method_name, method_type, method_param)
        definition += " {\n"
        if method_type != "void":
            definition += "    unsigned int inp_size = 3000;\n"
            definition += "    char * inp_var = malloc(inp_size);\n"
            definition += "    fgets(inp_var, inp_size, stdin);\n"

            definition += "    return *((" + method_type + "*) parse_inp(inp_var));\n"
        definition += " }\n"
        return definition


class AflTestConverter(TestConverter):
    @staticmethod
    def _get_test_name(test_file):
        return os.path.basename(test_file)

    def get_test_cases_in_dir(self, directory=None, exclude=()):
        if directory is None:
            directory = "."
        # 'crashes' and 'hangs' cannot lead to an error as long as we don't abort
        # in __VERIFIER_error()
        interesting_subdirs = (os.path.join(directory, d) for d in ["queue"])
        tcs = []
        for s in interesting_subdirs:
            abs_dir = os.path.abspath(s)
            assert os.path.exists(abs_dir), "Directory doesn't exist: %s" % abs_dir
            for t in glob.glob(abs_dir + "/id:*"):
                test_name = self._get_test_name(t)
                if test_name not in exclude and not test_name.endswith(".license"):
                    tcs.append(self.get_test_case_from_file(t))
        return tcs

    def get_test_case_from_file(self, test_file):
        test_name = self._get_test_name(test_file)
        with open(test_file, "rb") as inp:
            content = inp.read()
        return utils.TestCase(test_name, test_file, content)

    def get_test_vector(self, test_case):
        vector = utils.TestVector(test_case.name, test_case.origin)
        for line in test_case.content.split(b"\n"):
            vector.add(line)
        return vector
