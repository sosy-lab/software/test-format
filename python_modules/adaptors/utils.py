# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import logging
import os
import re

EXTERNAL_DECLARATIONS = """
struct _IO_FILE;
typedef struct _IO_FILE FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stderr;
typedef long unsigned int size_t;
extern void abort (void) __attribute__ ((__nothrow__ , __leaf__))
    __attribute__ ((__noreturn__));
extern void exit (int __status) __attribute__ ((__nothrow__ , __leaf__))
     __attribute__ ((__noreturn__));
extern char *fgets (char *__restrict __s, int __n, FILE *__restrict __stream);
extern int sscanf (const char *__restrict __s,
    const char *__restrict __format, ...) __attribute__ ((__nothrow__ , __leaf__));
extern size_t strlen (const char *__s)
    __attribute__ ((__nothrow__ , __leaf__))
    __attribute__ ((__pure__)) __attribute__ ((__nonnull__ (1)));
extern int fprintf (FILE *__restrict __stream,
    const char *__restrict __format, ...);
extern void *malloc (size_t __size) __attribute__ ((__nothrow__ , __leaf__))
    __attribute__ ((__malloc__));
extern void *memcpy (void *__restrict __dest, const void *__restrict __src,
    size_t __n) __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1, 2)));
extern long double strtold (const char *__restrict __nptr,
       char **__restrict __endptr)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern long long int strtoll (const char *__restrict __nptr,
         char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
extern unsigned long long int strtoull (const char *__restrict __nptr,
     char **__restrict __endptr, int __base)
     __attribute__ ((__nothrow__ , __leaf__)) __attribute__ ((__nonnull__ (1)));
"""

ERROR_STRING = "Error found."


class TestCase:
    def __init__(self, name, origin_file, content):
        self._name = name
        self._origin = os.path.abspath(origin_file)
        self._content = content

    @property
    def name(self):
        return self._name

    @property
    def origin(self):
        return self._origin

    @property
    def content(self):
        return self._content

    def __str__(self):
        return self.name + "(" + self.origin + ")"


class TestVector:
    """Test vector.

    Consists of a unique name, the original file that
    describes the test vector,
    and the vector as a sequence of test inputs.
    Each test input is a dictionary and consists
    of a 'value' and a 'name'.
    """

    def __init__(self, name, origin_file):
        self.name = name
        self.origin = origin_file
        self._vector = []

    def add(self, value, method=None):
        self._vector.append({"value": value, "name": method})

    @property
    def vector(self):
        """The sequence of test inputs of this test vector.

        Each element of this sequence is a dict
        and consists of two entries: 'value' and 'name'.
        The 'value' entry describes the input value, as it should be given
        to the program as input.
        The 'name' entry describes the program input method
        through which the value is retrieved. The value of this entry may be None.
        """
        return self._vector

    def __len__(self):
        return len(self.vector)

    def __str__(self):
        return self.origin + " " + str(self.vector)


def get_assume_method():
    return "void __VERIFIER_assume(int cond) {\n    if(!cond) {\n        abort();\n    }\n}\n"


def get_error_method_definition(error_method):
    return (
        "void "
        + error_method
        + '() {{ fprintf(stderr, "{0}\\n"); exit(1); }}\n'.format(ERROR_STRING)
    )


def get_method_head(method_name, method_type, param_types):
    method_head = "{0} {1}(".format(method_type, method_name)
    params = []
    for (idx, param_type) in enumerate(param_types):
        if "..." in param_type:
            params.append("...")
        elif param_type != "void":
            if "{}" not in param_type:
                param_type += " {}"
            params.append(param_type.format("param{}".format(idx)))
        elif params:
            raise AssertionError("Void type parameter in method " + method_name)
    method_head += ", ".join(params)
    method_head += ")"
    return method_head


def find_nondet_methods(file_content, excludes=()):
    """Return non-deterministic methods in given string.

    :param str file_content: string to check for non-deterministic methods
    :param List[str] excludes: list of method definitions to regard as deterministic
    :return: list of dictionaries, with one dictionary per non-deterministic method.
        Each dictionary contains three keys: 'name', 'type' and 'params'.
        'name' is the name of the method,
        'type' is the return type of the method,
        'params' is a list of the types of the method parameters.
    """
    nondet_pattern = re.compile(r"__VERIFIER_nondet_.+?\(\)")
    if os.path.exists(file_content):
        with open(file_content, "r", encoding="UTF-8") as inp:
            content = inp.read()
    else:
        content = file_content
    method_names = set(
        s[:-2] for s in nondet_pattern.findall(content) if s[:-2] not in excludes
    )

    functions = []
    for method_name in method_names:
        method_type = _get_return_type(method_name)
        functions.append({"name": method_name, "type": method_type, "params": []})
    return functions


def _get_return_type(verifier_nondet_method):
    assert verifier_nondet_method.startswith("__VERIFIER_nondet_")
    assert verifier_nondet_method[-2:] != "()"
    m_type = verifier_nondet_method[len("__VERIFIER_nondet_") :].lower()
    if m_type == "bool":
        m_type = "_Bool"
    elif m_type == "u32":
        m_type = "unsigned int"
    elif m_type == "u16":
        m_type = "unsigned short"
    elif m_type == "u8":
        m_type = "unsigned char"
    elif m_type == "unsigned":
        # unsigned is a synonym for unsigned int, so recall the method with that
        m_type = "unsigned int"
    elif m_type[0] == "u":  # resolve uint to unsigned int (e.g.)
        m_type = "unsigned " + m_type[1:]
    elif m_type == "pointer":
        m_type = "void *"
    elif m_type == "pchar":
        m_type = "char *"
    elif m_type == "s8":
        m_type = "char"
    return m_type


def get_sym_var_name(method_name):
    sym_var_prefix = "__sym_"
    name = sym_var_prefix + method_name
    logging.debug("Getting sym var name for method %s: %s", method_name, name)
    return name
