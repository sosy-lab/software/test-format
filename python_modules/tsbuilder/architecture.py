# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# Copyright (C) 2018  Dirk Beyer
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

"""Constants for common system architectures"""

LINUX32 = "32bit"
"""Linux 32 bit architecture."""

LINUX64 = "64bit"
"""Linux 64 bit architecture."""
